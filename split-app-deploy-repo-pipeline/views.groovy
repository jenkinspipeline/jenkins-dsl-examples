

def application = 'application'
def base_branch = 'master'
def base_jobpath = 'build'


deliveryPipelineView("${application}/${base_branch}-deployment") {
    pipelineInstances(5)
    columns(2)
    sorting(Sorting.TITLE)
    updateInterval(60)
    allowPipelineStart()
    allowRebuild()
    enableManualTriggers()
    showAvatars()
    showChangeLog()
    showTestResults()
    showTotalBuildTime()
    pipelines {
        component('Application', "${application}/deploy/${base_branch}/01-collect-changeset")
    }
}

deliveryPipelineView("${application}/${base_branch}-build") {
    pipelineInstances(5)
    columns(2)
    sorting(Sorting.TITLE)
    updateInterval(60)
    allowPipelineStart()
    allowRebuild()
    enableManualTriggers()
    showAvatars()
    showChangeLog()
    showTestResults()
    showTotalBuildTime()
    pipelines {
        component('Application', "${application}/build/${base_branch}/01-test")
    }
}
