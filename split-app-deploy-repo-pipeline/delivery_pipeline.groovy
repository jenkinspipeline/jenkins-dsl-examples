import templates.BaseJob
import templates.ChainedJob


def application = 'application'
def base_branch = 'master'
def base_jobpath = 'deploy'
def deploy_branch = 'master'
def deploy_jobpath = 'application-pipeline-new'
def slack_notify_room = "#application-notifications"

def application_git_url = "git@bitbucket.org:jenkinspipeline/example-application.git"
def application_git_web_url = 'https://bitbucket.org/jenkinspipeline/example-application'
def application_git_credentials = 'bitbucket'

def deploy_git_url = "git@bitbucket.org:jenkinspipeline/example-deploy.git"
def deploy_git_web_url = 'https://bitbucket.org/jenkinspipeline/example-deploy'
def deploy_git_credentials = 'bitbucket'


folder(application) {}
folder("${application}/${base_jobpath}") {}
folder("${application}/${base_jobpath}/${base_branch}") {}

def buildJob = job("${application}/${base_jobpath}/${base_branch}/01-collect-changeset")

BaseJob.configureDefaults(buildJob, "Build server image", "Prepare version", slack_notify_room)

buildJob.with {

    label('master')

    triggers {
        scm('* * * * *')
    }

    scm {
        git {
            remote {
                name('origin')
                url(deploy_git_url)
                credentials(deploy_git_credentials)
            }
            browser {
                stash(deploy_git_web_url)
            }
            branch(base_branch)
            extensions {
                localBranch(base_branch)
                pathRestriction {
                    includedRegions("")
                    excludedRegions("VERSION")
                }
            }
        }
    }

    steps {

        shell(
"""#!/bin/bash
ops/ci/prepare_version.sh
"""
        )

        envInjectBuilder {
            propertiesFilePath("build.properties")
            propertiesContent("")
        }
    }

    publishers {
        archiveArtifacts('**')

        downstreamParameterized {
            trigger("${application}/${base_jobpath}/${base_branch}/02-lint-files") {
                parameters {
                    currentBuild()
                    predefinedProp('ARTIFACT_BUILD_NUMBER', '$ARTIFACT_BUILD_NUMBER')
                    predefinedProp('GIT_COMMIT', '$GIT_COMMIT')
                }
            }
        }

        gitPublisher {
            branchesToPush {
                branchToPush {
                    targetRepoName("origin")
                    branchName(base_branch)
                }
            }

            pushMerge(false)
            forcePush(false)
            pushOnlyIfSuccess(true)
        }
    }
}

def lintJob = job("${application}/${base_jobpath}/${base_branch}/02-lint-files")

ChainedJob.addDeploy(lintJob, "Build server image", "Lint files", slack_notify_room, deploy_git_url, deploy_git_credentials, deploy_git_web_url)
BaseJob.addDownstreamJobs(lintJob, "${application}/${base_jobpath}/${base_branch}/02-packer", null)

lintJob.with {

    label('base-agent')

    steps {

        shell(
"""#!/bin/bash
ops/ci/lint_files.sh
"""
        )
    }
}

def packerJob = job("${application}/${base_jobpath}/${base_branch}/02-packer")

ChainedJob.addDeploy(packerJob, "Build server image", "Build and Publish", slack_notify_room, deploy_git_url, deploy_git_credentials, deploy_git_web_url)
BaseJob.addDownstreamJobs(packerJob, "${application}/${base_jobpath}/${base_branch}/deploy-to-dev,${application}/${base_jobpath}/${base_branch}/03-verify-image", null)

packerJob.with {

    label('base-agent')

    steps {

        envInjectBuilder {
            propertiesFilePath("ops/version/build.properties")
            propertiesContent("")
        }

        shell(
"""#!/bin/bash
ops/ci/build_artifact.sh
"""
        )
    }

    publishers {
        gitPublisher {
            tagsToPush {
                tagToPush {
                    targetRepoName("origin")
                    tagName('$ARTIFACT_BUILD_NUMBER')
                    tagMessage('Tagging for release $ARTIFACT_BUILD_NUMBER')
                    updateTag(false)
                    createTag(true)
                }
            }
            pushMerge(false)
            forcePush(false)
            pushOnlyIfSuccess(true)
        }
    }
}


def verifyJob = job("${application}/${base_jobpath}/${base_branch}/03-verify-image")

ChainedJob.addDeploy(verifyJob, "Build server image", "Verify server image", slack_notify_room, deploy_git_url, deploy_git_credentials, deploy_git_web_url)

verifyJob.with {

    label('base-agent')

    wrappers {
        credentialsBinding {
            file {
                variable("KITCHEN_SSH_KEY_PEM")
                credentialsId("jenkins-aws-ssh-key")
            }
        }
    }

    steps {

        shell(
"""#!/bin/bash
ops/ci/verify_image.sh
"""
        )
    }
}

def enviromentJobs = { jobpath, branch_name, environment, nextEnvironment, pushGitChanges, closure ->

    auto_downstream = "${application}/${jobpath}/${branch_name}/regression-test-${environment}"
    manual_downstream = null

    def deployJob = job("${application}/${jobpath}/${branch_name}/deploy-to-${environment}")

    ChainedJob.addDeploy(deployJob, "Deploy to ${environment}", "Deploy Application", slack_notify_room, deploy_git_url, deploy_git_credentials, deploy_git_web_url)
    BaseJob.addDownstreamJobs(deployJob, auto_downstream, manual_downstream)

    deployJob.with {

        label('base-agent')

        steps {

            shell(
"""#!/bin/bash
ops/ci/deploy_to_${environment}.sh
"""
            )
        }

        publishers {
            if (pushGitChanges) {
                gitPublisher {
                    branchesToPush {
                        branchToPush {
                            targetRepoName("origin")
                            branchName(branch_name)
                        }
                    }

                    pushMerge(false)
                    forcePush(false)
                    pushOnlyIfSuccess(true)
                }
            }
        }
    }

    auto_downstream = null
    manual_downstream = null

    if (nextEnvironment) {
        manual_downstream = "${application}/${jobpath}/${branch_name}/deploy-to-${nextEnvironment}, ${application}/${jobpath}/${branch_name}/load-test-${environment}, ${application}/${jobpath}/${branch_name}/security-test-${environment}"
    }

    def regressionJob = job("${application}/${jobpath}/${branch_name}/regression-test-${environment}")

    ChainedJob.addDeploy(regressionJob, "Deploy to ${environment}", "Regression Test", slack_notify_room, deploy_git_url, deploy_git_credentials, deploy_git_web_url)
    BaseJob.addDownstreamJobs(regressionJob, auto_downstream, manual_downstream)

    regressionJob.with {

        label('chromedriver')

        steps {
            shell(
"""#!/bin/bash
ops/ci/regression_test_${environment}.sh
"""
            )
        }

        publishers {
            archiveJunit('regression-test/reports/*.xml')
        }
    }

    def loadTestJob = job("${application}/${jobpath}/${branch_name}/load-test-${environment}")
    ChainedJob.addDeploy(loadTestJob, "Deploy to ${environment}", "Perform Load Test", slack_notify_room, deploy_git_url, deploy_git_credentials, deploy_git_web_url)

    loadTestJob.with {
        label('base-agent')

        steps {
            shell(
"""#!/bin/bash
ops/ci/load_test_${environment}.sh
"""
            )
        }
    }

    def securityTestJob = job("${application}/${jobpath}/${branch_name}/security-test-${environment}")
    ChainedJob.addDeploy(securityTestJob, "Deploy to ${environment}", "Perform Security Test", slack_notify_room, deploy_git_url, deploy_git_credentials, deploy_git_web_url)

    securityTestJob.with {
        label('base-agent')

        steps {
            shell(
"""#!/bin/bash
ops/ci/security_test_${environment}.sh
"""
            )
        }
    }
}

enviromentJobs(base_jobpath, base_branch, "dev", "qat", false) {}
enviromentJobs(base_jobpath, base_branch, "qat", "prod", false) {}
enviromentJobs(base_jobpath, base_branch, "prod", null, true) {}
