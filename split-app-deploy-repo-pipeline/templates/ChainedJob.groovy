package templates

class ChainedJob {

    static private void addCommon(def job, def stage, def task, def slack_room, def git_url, def git_credentials, def git_web_url) {

        BaseJob.configureDefaults(job, stage, task, slack_room)

        job.with {

            parameters {
                stringParam('GIT_COMMIT', '', 'The GIT commit to build.')
            }

            scm {
                git {
                    remote {
                        name('origin')
                        url(git_url)
                        credentials(git_credentials)
                    }
                    browser {
                        stash(git_web_url)
                    }
                    branch('\${GIT_COMMIT}')
                }
            }

        }
    }

    static void addDeploy(def job, def stage, def task, def slack_room, def git_url, def git_credentials, def git_web_url) {

        addCommon(job, stage, task, slack_room, git_url, git_credentials, git_web_url)

        job.with {
            parameters {
                stringParam('ARTIFACT_BUILD_NUMBER', '', 'The build label to deploy.')
            }
        }
    }

    static void addBuild(def job, def stage, def task, def slack_room, def git_url, def git_credentials, git_web_url) {

        addCommon(job, stage, task, slack_room, git_url, git_credentials, git_web_url)
    }
}
