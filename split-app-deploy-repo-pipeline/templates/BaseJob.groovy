package templates

class BaseJob {

    static void configureDefaults(def job, def stage, def task, def slack_room) {

        job.with {

            label('base-agent')
            deliveryPipelineConfiguration(stage, task)

            authorization {
                permission('hudson.model.Item.Build', 'testing')
            }

            wrappers {
                colorizeOutput()
            }

            publishers {
                if (slack_room) {
                    slackNotifier {
                        room(slack_room)
                        notifyAborted(true)
                        notifyFailure(true)
                        notifyNotBuilt(true)
                        notifyUnstable(true)
                        notifyBackToNormal(true)
                        notifySuccess(false)
                        notifyRepeatedFailure(true)
                        startNotification(false)
                        includeTestSummary(true)
                        commitInfoChoice('AUTHORS_AND_TITLES')
                    }
                }
            }
        }
    }

    static void pollGitAsMaster(def job, def application_git_url, def application_git_credentials, def application_git_web_url) {

        job.with {
            triggers {
                scm('* * * * *')
            }

            scm {
                git {
                    remote {
                        name('origin')
                        url(application_git_url)
                        credentials(application_git_credentials)
                    }
                    browser {
                        stash(application_git_web_url)
                    }
                    branch('master')
                }
            }
        }
    }


    static void addDownstreamJobs(def job, def auto_downstreamjob, def manual_downstreamjob, def properties_file = null) {

        job.with {

            publishers {
                if (auto_downstreamjob) {
                    downstreamParameterized {
                        trigger(auto_downstreamjob) {
                            parameters {
                                currentBuild()
                                if (properties_file){
                                    propertiesFile(properties_file)
                                }
                            }
                        }
                    }
                }

                if (manual_downstreamjob) {
                    buildPipelineTrigger(manual_downstreamjob) {
                        parameters {
                            currentBuild()
                        }
                    }
                }
            }
        }

    }
}
