import templates.BaseJob
import templates.ChainedJob


def application = 'application'
def base_branch = 'master'
def base_jobpath = 'build'
def deploy_branch = 'master'
def deploy_jobpath = 'application-pipeline-new'
def slack_notify_room = "#application-notifications"

def application_git_url = "git@bitbucket.org:jenkinspipeline/example-application.git"
def application_git_web_url = 'https://bitbucket.org/jenkinspipeline/example-application'
def application_git_credentials = 'bitbucket'

def deploy_git_url = "git@bitbucket.org:jenkinspipeline/example-deploy.git"
def deploy_git_web_url = 'https://bitbucket.org/jenkinspipeline/example-deploy'
def deploy_git_credentials = 'bitbucket'



folder(application) {}
folder("${application}/${base_jobpath}") {}
folder("${application}/${base_jobpath}/${base_branch}") {}

def buildJob = job("${application}/${base_jobpath}/${base_branch}/01-test")

BaseJob.configureDefaults(buildJob, "Build application", "Test, Package and Publish", slack_notify_room)
BaseJob.pollGitAsMaster(buildJob, application_git_url, application_git_credentials, application_git_web_url)
BaseJob.addDownstreamJobs(buildJob, "${application}/${base_jobpath}/${base_branch}/02-deploy", null, "env.properties")

buildJob.with {

    steps {

        shell(
"""
ops/ci/build_artifact.sh
ops/ci/publish_artifact.sh
"""
        )

        envInjectBuilder {
            propertiesFilePath("env.properties")
            propertiesContent("")
        }
    }
}

def deployJob = job("${application}/${base_jobpath}/${base_branch}/02-deploy")

ChainedJob.addBuild(deployJob, "Deploy", "Schedule for deploy", slack_notify_room, deploy_git_url, deploy_git_credentials, deploy_git_web_url)

deployJob.with {

    parameters {
        stringParam('PACKAGE_NAME', '', 'The package name.')
    }

    steps {
        shell(
"""
ops/ci/promote_package_version.sh
"""
        )
    }

    publishers {
        gitPublisher {
            branchesToPush {
                branchToPush {
                    targetRepoName("origin")
                    branchName('master')
                }
            }
            pushMerge(false)
            forcePush(false)
            pushOnlyIfSuccess(true)
        }
    }

}
